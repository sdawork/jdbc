package pl.sdacademy.jdbc.dao;

import pl.sdacademy.jdbc.db.DBUtil;
import pl.sdacademy.jdbc.domain.User;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserDAO {

	public List<User> findAll() {
		List<User> users = new ArrayList<>();
		try (
				Connection connection = DBUtil.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM users")
		) {
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setFirstname(resultSet.getString("firstname"));
				user.setLastname(resultSet.getString("lastname"));
				users.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;
	}

	public Optional<User> findOne(int id) {
		// TODO:
		return null;
	}

	public List<User> findAllSortByFirstname() {
		// TODO:
		return null;
	}

	public List<User> findAllSortByLastname() {
		// TODO:
		return null;
	}

	public List<User> findAllNewestFirst() {
		// TODO:
		return null;
	}


	public boolean createUser(String firstname, String lastname) {
		// TODO:
		return false;
	}

/*	public User createUser(String firstname, String lastname) {
		// TODO:
		// hint: https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-usagenotes-last-insert-id.html
		return null;
	}*/

	public void modifyUser(int id, String newFirstname, String newLastname) {
		// TODO:
	}

	public void removeUserByFirstname(String firstname) {
		// TODO:
	}

}
