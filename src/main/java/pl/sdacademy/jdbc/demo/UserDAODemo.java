package pl.sdacademy.jdbc.demo;

import pl.sdacademy.jdbc.dao.UserDAO;
import pl.sdacademy.jdbc.domain.User;

import java.util.List;

public class UserDAODemo {

	public static void main(String[] args) {
		final UserDAO userDAO = new UserDAO();
		final List<User> all = userDAO.findAll();
		System.out.println("all = " + all);
	}
}
