package pl.sdacademy.jdbc.demo;

import pl.sdacademy.jdbc.db.DBUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Users {

	public static void main(String[] args) {

		try (
				Connection connection = DBUtil.getConnection();
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM users")
		) {
			while (resultSet.next()) {
				System.out.println(resultSet.getInt("id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
