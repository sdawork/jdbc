CREATE DATABASE jdbc DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;
use jdbc;

CREATE USER 'user_jdbc'@'localhost' IDENTIFIED BY 'jdbc01';
GRANT ALL PRIVILEGES ON jdbc.* TO 'user_jdbc'@'localhost';
FLUSH PRIVILEGES;

CREATE TABLE users
(
  id        INT          NOT NULL AUTO_INCREMENT,
  firstname VARCHAR(255) NOT NULL,
  lastname  VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO users (firstname, lastname)
VALUES ('Jan', 'Kowalski'),
       ('Karol', 'Malinowski'),
       ('Anna', 'Perucka'),
       ('Tomasz', 'Walicki'),
       ('Katarzyna', 'Kozak'),
       ('Janusz', 'Bagiński'),
       ('Monika', 'Zakrzewska'),
       ('Tomasz', 'Kot');


CREATE TABLE tasks
(
  id           INT         NOT NULL AUTO_INCREMENT,
  title        VARCHAR(45) NOT NULL,
  priority     INT         NOT NULL,
  pinned       BOOL        NOT NULL,
  created_date DATE        NOT NULL,
  owner_id     INT         NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT owner_id FOREIGN KEY (id) REFERENCES users (id)
);
